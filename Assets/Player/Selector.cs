﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : MonoBehaviour {

	public GameObject Player;
	public Transform tip;
	private LineRenderer line;
    public GameObject Flashlight;

    public GameEventManager GameEventManager;
    public static FishData current;
    public HUDManager HUDManager;
    public Terrain terrain;
    RaycastHit hit;
    public GameObject scanned_fish;
    public bool scanned;
    public AudioSource scan_comfirm_sound;
    public AudioSource action_sound;

	//public GameObject tablet;

    public enum PLAYER_STATE {
        S_SCANNING,
        S_INTERACTING,
    };

	// Use this for initialization
	void Start () {
        scanned = false;

        line = GetComponent<LineRenderer>();
		line.startWidth = 0.01f;
		line.startColor = Color.white;
		line.endColor = Color.blue;
		line.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        OVRInput.Update();

        if (OVRInput.GetDown(OVRInput.RawButton.RThumbstick, OVRInput.Controller.RTouch))
        {
            //reference
        }

        //Player movement up and down
        if (Player.transform.position.y < terrain.SampleHeight(new Vector3(0,0,0)) - 5f);
        {
            if (OVRInput.Get(OVRInput.RawAxis2D.RThumbstick, OVRInput.Controller.RTouch).y > 0.2f && OVRInput.Get(OVRInput.RawAxis2D.RThumbstick, OVRInput.Controller.RTouch).y <= 0.7f)
            {
                CharacterInteraction chrctr = GameEventManager.character.GetComponent<CharacterInteraction>();
                chrctr.MoveToPlayer();
                //up slower
                Player.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y+0.01f, Player.transform.position.z);
            }
            else if(OVRInput.Get(OVRInput.RawAxis2D.RThumbstick, OVRInput.Controller.RTouch).y > 0.7f)
            {
                CharacterInteraction chrctr = GameEventManager.character.GetComponent<CharacterInteraction>();
                chrctr.MoveToPlayer();
                //up faster
                if (Player.transform.position.y <= 200)
                    Player.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y+0.1f, Player.transform.position.z);
                else
                    Player.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y+0.05f, Player.transform.position.z);
            }
        }
        if (Player.transform.position.y > 3f)
        {
            if (OVRInput.Get(OVRInput.RawAxis2D.RThumbstick, OVRInput.Controller.RTouch).y < -0.2f && OVRInput.Get(OVRInput.RawAxis2D.RThumbstick, OVRInput.Controller.RTouch).y >= -0.7f)
            {
                CharacterInteraction chrctr = GameEventManager.character.GetComponent<CharacterInteraction>();
                chrctr.MoveToPlayer();
                //down slower
                Player.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y-0.01f, Player.transform.position.z);
            }
            else if(OVRInput.Get(OVRInput.RawAxis2D.RThumbstick, OVRInput.Controller.RTouch).y < -0.7f)
            {
                CharacterInteraction chrctr = GameEventManager.character.GetComponent<CharacterInteraction>();
                chrctr.MoveToPlayer();
                //down faster
                if (Player.transform.position.y <= 200)
                    Player.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y-0.1f, Player.transform.position.z);
                else
                    Player.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y-0.05f, Player.transform.position.z);
            }
        }

        //If RHandTigger, activate ray, if A pressed, shoot raycast and select target fish (also for event manager)
        if (OVRInput.Get(OVRInput.RawAxis1D.RHandTrigger, OVRInput.Controller.RTouch) > 0.1f)
        {
            if (!line.enabled)
                line.enabled = true;
            line.SetPosition(0, tip.position);
            if (scanned && scanned_fish != null) {
                line.SetPosition(1, scanned_fish.transform.position);
                line.startColor = Color.green;
            }
            else {
                line.startColor = Color.white;
                if (Physics.Raycast(tip.position, this.transform.forward * 300f, out hit))
                    line.SetPosition(1, hit.point);
                else
                    line.SetPosition(1, tip.position + (this.transform.forward * 300f));
            }
            //Raycast and pass target fish to event manager
            if (OVRInput.Get(OVRInput.RawButton.A, OVRInput.Controller.RTouch) && !scanned) {
                Debug.Log("A Button");
                ShootRaycast();
            }
        }
        else
        {
            if (line.enabled)
                line.enabled = false;
            scanned = false;
            scanned_fish = null;
        }
        //Action Button Events for non scanning?
        if (OVRInput.GetDown(OVRInput.RawButton.B, OVRInput.Controller.RTouch)) 
        {
            Debug.Log("B Button");
            action_sound.Play();
            GameEventManager.ActionButtonPressed();
        }
        

        //Flashlight
        //MLT: 
        if (OVRInput.Get(OVRInput.RawAxis1D.RIndexTrigger, OVRInput.Controller.RTouch) > 0.01f)
        {
            Flashlight.SetActive(true);
            if (OVRInput.GetDown(OVRInput.RawButton.B, OVRInput.Controller.RTouch))
            {
                //select
                //GameEventManager.TargetSelection();//.ActionButtonPressed("Flashlight+B");
            }
        }
        else
        {
            Flashlight.SetActive(false);
        }
	}

    //Scan
    public bool ShootRaycast()
    {
        if (Physics.Raycast(tip.position, this.transform.forward * 300f, out hit))
        {
            line.SetPosition(1, hit.point);
            if (hit.collider.tag == "Fish")
            {
                line.startColor = Color.green;
                scanned = true;
                scanned_fish = hit.collider.gameObject;
                // assume gameobject.name is name of fishdata entry (no spaces)
                string name = scanned_fish.name;
                //toggle the .discovered value
                GenerateFactList.fishfacts[name].discovered = true;
                //call findFish() with name to update HUD to that fish's page
                HUDManager.findFish(name);
                //call TargetSelection() to update quest
                GameEventManager.TargetSelection(scanned_fish.transform);
                scan_comfirm_sound.Play();

                return true;
            }
        }
        else
        {
            line.startColor = Color.white;
            scanned = false;
            Debug.Log("No hit");
        }
        return false;
    }
}