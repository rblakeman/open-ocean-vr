﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControllers : MonoBehaviour {

    public GameObject righthand;
    public GameObject lefthand;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        OVRInput.Update();
        righthand.transform.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);
        righthand.transform.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTouch);
        lefthand.transform.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
        lefthand.transform.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.LTouch);
    }
}
