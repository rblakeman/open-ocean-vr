﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIInfoPage : MonoBehaviour {

    public void UpdatePage(string fishid)
    {
        FishData entry;
        GenerateFactList.fishfacts.TryGetValue(fishid, out entry);
        if (entry != null)
        {
            Text name = transform.Find("Name").GetComponentInChildren<Text>();
            name.text = entry.fishname;
            Image p = transform.Find("Picture").GetComponentInChildren<Image>();
            p.sprite = entry.icon;
            Text sname = transform.Find("ScientificName").GetComponentInChildren<Text>();
            sname.text = "Scientific Name: " + entry.scientificname;
            Text size = transform.Find("Size").GetComponentInChildren<Text>();
            size.text = "Size: " + entry.size;
            Text habitat = transform.Find("Habitat").GetComponentInChildren<Text>();
            habitat.text = "Habitat: " + entry.habitat;
            Text type = transform.Find("Type").GetComponentInChildren<Text>();
            type.text = "Type: " + entry.animaltype;
            Text diet = transform.Find("Diet").GetComponentInChildren<Text>();
            diet.text = "Diet: " + entry.diet;
            Text relatives = transform.Find("Relatives").GetComponentInChildren<Text>();
            relatives.text = "Relatives: " + entry.relative;
            Text range = transform.Find("Range").GetComponentInChildren<Text>();
            range.text = "Range: " + entry.range;
        }

        
    }
}
