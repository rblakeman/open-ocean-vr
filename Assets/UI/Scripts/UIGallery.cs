﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIGallery : MonoBehaviour {
    

    public GameObject layout;
    public GameObject species;
    public Transform content;
    Dictionary<string, FishData>.KeyCollection entries;

    public Sprite anonymous;

    public int boxindex;
    public string sid;
    bool turn;

    public AudioSource cursor_move;

    // Use this for initialization
    void Start () {
        entries = GenerateFactList.fishfacts.Keys;
        
        content = layout.GetComponentInChildren<Transform>(); //content = transform.Find("ScrollView/Viewport/Content").GetComponentInChildren<Transform>();
        content.localPosition = new Vector3(-8, 225, 0);

        turn = false;
        boxindex = 0;
        GalleryUpdate();
    }
	
	// Update is called once per frame
	void Update () {
        OVRInput.Update(); //Touch Controller input
        GalleryControls();
    }

    public void GalleryUpdate()
    {
        if (content.childCount > 0)
        {
            foreach (Transform child in content)
            {
                GameObject.Destroy(child.gameObject);
            }
        }

        int innerindex = 0;
        FishData entry;
        entries = GenerateFactList.fishfacts.Keys;
        foreach (string data in entries)
        {
            GenerateFactList.fishfacts.TryGetValue(data, out entry);
            GameObject s = Instantiate(species, Vector2.zero, Quaternion.identity);
            s.transform.SetParent(layout.transform, false);
            Text name = s.GetComponentInChildren<Text>();
            Image p = s.transform.Find("Picture").GetComponentInChildren<Image>();

            if (entry.discovered == false)
            {
                name.text = "";
                for(int ii = 0; ii < entry.fishname.Length; ii++)
                {
                    char letter = entry.fishname[ii];
                    if(letter != ' ')
                    {
                        name.text += "-";
                    }
                    else{
                        name.text += letter;
                    }
                }
                p.sprite = anonymous;
            }
            else
            {
                name.text = entry.fishname;
                p.sprite = entry.icon;
            }

            if(boxindex == innerindex)
            {
                s.GetComponent<Image>().color = new Color32(255, 255, 0, 100);
                s.GetComponent<Image>().fillCenter = true;
                if (entry.discovered == true)
                {
                   sid = data;
                } 
                else
                {
                   sid = null;
                }
            }
            innerindex += 1;
        }
    }

     void GalleryControls()
    {
        int hnum = boxindex % 3;
        int vnum = boxindex;

        //Turning joystick to the right
        if(OVRInput.Get(OVRInput.RawAxis2D.LThumbstick, OVRInput.Controller.LTouch).x > 0.2f)
        {
            if(!turn)
            {
                cursor_move.Play();
                switch (hnum)
                {
                    case 0:
                        boxindex++;
                        if (boxindex >= entries.Count)
                            boxindex--;
                        GalleryUpdate();
                        break;
                    case 1:
                        boxindex++;
                        if (boxindex >= entries.Count)
                            boxindex -= 2;
                        GalleryUpdate();
                        break;
                    case 2:
                        boxindex = boxindex - 2;
                        GalleryUpdate();
                        break;
                }
                
                turn = true;
            }
        }
        else if(OVRInput.Get(OVRInput.RawAxis2D.LThumbstick, OVRInput.Controller.LTouch).x < -0.2f)
        {
            if(!turn)
            {
                cursor_move.Play();
                switch (hnum)
                {
                    case 0:
                        boxindex = boxindex + 2;
                        if (boxindex >= entries.Count)
                            boxindex --;
                        GalleryUpdate();
                        break;
                    case 1:
                        boxindex--;
                        GalleryUpdate();
                        break;
                    case 2:
                        boxindex--;
                        GalleryUpdate();
                        break;
                }
                
                turn = true;
            }
        }
        else if(OVRInput.Get(OVRInput.RawAxis2D.LThumbstick, OVRInput.Controller.LTouch).y > 0.2f)
        {
            if(!turn)
            {
                cursor_move.Play();
                vnum = vnum - 3;
                if(vnum >= 0)
                {
                    boxindex -= 3;
                    GalleryUpdate();
                    //HUDPosition(boxindex);
                }
                
                turn = true;
            }
            
        }
        else if(OVRInput.Get(OVRInput.RawAxis2D.LThumbstick, OVRInput.Controller.LTouch).y < -0.2f)
        {
            if(!turn)
            {
                cursor_move.Play();
                vnum = vnum + 3;
                if (vnum < entries.Count)
                {
                    boxindex += 3;
                    GalleryUpdate();
                }
                //HUDPosition(vnum);
                
            }
            turn = true;
        }
        else
        {
            turn = false;
        }
    }

    void HUDPosition(int index)
    {
        float currentPos = content.localPosition.y;
        if(currentPos > 220 && currentPos < 230)
        {
            if(index >= 6)
                content.localPosition = new Vector3(-8, 450, 0);
        }
        else if(currentPos > 445 && currentPos < 455)
        {
            if(index < 3)
                content.localPosition = new Vector3(-8, 225, 0);
            else if(index >= 9)
                content.localPosition = new Vector3(-8, 675, 0);
        }
        else if(currentPos > 670 && currentPos < 680)
        {
            if (index < 6)
                content.localPosition = new Vector3(-8, 450, 0);
            else if(index >= 12)
                content.localPosition = new Vector3(-8, 900, 0);
        }
        else if(currentPos > 895 && currentPos < 905)
        {
            if (index < 9)
                content.localPosition = new Vector3(-8, 675, 0);
            else if (index >= 15)
                content.localPosition = new Vector3(-8, 1125, 0);
        }
        else if(currentPos > 1120 && currentPos < 1130)
        {
            if(index < 12)
                content.localPosition = new Vector3(-8, 900, 0);
        }
    }
}
