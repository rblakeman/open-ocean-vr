﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIQuest : MonoBehaviour {

	public void UpdatePage()
    {
        GameEvent current = GameEventManager.currentEvent;

        if (current != null)
        {
            Text name = transform.Find("QuestName").GetComponentInChildren<Text>();
            name.text = current.eventName;
            Image p = transform.Find("Picture").GetComponentInChildren<Image>();
            p.sprite = current.eventIcon;
            Text info = transform.Find("QuestInfo").GetComponentInChildren<Text>();
            info.text = current.description;
            Text hint = transform.Find("QuestHint").GetComponentInChildren<Text>();
            hint.text = current.hint;
            
        }

        
    }
}
