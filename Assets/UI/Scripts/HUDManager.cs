﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDManager : MonoBehaviour {
    public GameObject collection;
    public GameObject gallery;

    public GameObject infopage;
    public string speciesid;

    public GameObject questpage;

    UIGallery gscript;
    UIInfoPage iscript;
    UIQuest qscript;

    GameObject defaultScreen;

    public GameEventManager GameEventManager;
    public Material watchlight;

    bool turn;

    public AudioSource hud_change;
    public AudioSource cursor_confirm;

    // Use this for initialization
    void Awake () {
        gallery.SetActive(false);
        infopage.SetActive(false);
        questpage.SetActive(false);
	}

    void Start()
    {
        gscript = gallery.GetComponent<UIGallery>();
        iscript = infopage.GetComponent<UIInfoPage>();
        qscript = questpage.GetComponent<UIQuest>();
        defaultScreen = gallery;
        watchlight.SetColor("_EmissionColor", new Color(0f,0f,0f));
        turn = false;
    }

    // Update is called once per frame
    void Update () {
        //Turns on the HUD
        if(OVRInput.Get(OVRInput.RawAxis1D.LHandTrigger, OVRInput.Controller.LTouch) > 0.01f)
        {
            GameEventManager.OpenMenuHUD();
            //Open and closes info page from the gallery
            if (OVRInput.Get(OVRInput.RawAxis1D.LIndexTrigger, OVRInput.Controller.LTouch) > 0.01f)
            {
                if(!turn)
                {
                    cursor_confirm.Play();
                    if (defaultScreen == infopage) 
                    {
                        defaultScreen = gallery;
                    }
                    else if(defaultScreen == gallery)
                    {
                        defaultScreen = infopage;
                        speciesid = gscript.sid;
                    }
                    turn = true;
                }
            }
            else if(OVRInput.Get(OVRInput.RawButton.X, OVRInput.Controller.LTouch))
            {
                if(!turn)
                {
                    hud_change.Play();
                    Debug.Log("X Button");
                    if(defaultScreen != questpage)
                    {
                        defaultScreen = questpage;
                    }
                    else if(defaultScreen == questpage)
                    {
                        defaultScreen = gallery;
                    }
                    turn = true;
                }
            }
            else if(OVRInput.Get(OVRInput.RawButton.Y, OVRInput.Controller.LTouch))
            {
                if(!turn)
                {
                    hud_change.Play();
                    Debug.Log("Y Button");
                    if(defaultScreen != questpage)
                    {
                        defaultScreen = questpage;
                    }
                    else if(defaultScreen == questpage)
                    {
                        defaultScreen = gallery;
                    }
                    turn = true;
                }
            }
            else
            {
                turn = false;
            }
            

            showScreen();

            watchlight.SetColor("_EmissionColor", new Color(0.6397059f, 0.7912779f, 1f));
            collection.SetActive(true);
            GameEventManager.OpenMenuHUD(true);
        }
        else
        {
            GameEventManager.OpenMenuHUD(false);
            collection.SetActive(false);
            watchlight.SetColor("_EmissionColor", new Color(0f,0f,0f));
            defaultScreen.SetActive(false);
        }
    }

    void showScreen()
    {
        //Activate the Screen Selected as the Default Screen
        if(defaultScreen == infopage)
        {
            gallery.SetActive(false);
            questpage.SetActive(false);
            infopage.SetActive(true);
            iscript.UpdatePage(speciesid);
        }
        else if(defaultScreen == gallery)
        {
            infopage.SetActive(false);
            questpage.SetActive(false);
            gallery.SetActive(true);
            gscript.GalleryUpdate();
        }
        else if(defaultScreen == questpage)
        {
            gallery.SetActive(false);
            infopage.SetActive(false);
            questpage.SetActive(true);
            qscript.UpdatePage();
        }
    
    }

    public void findFish(string sid)
    {
        defaultScreen = infopage;
        speciesid = sid;
    }
}
