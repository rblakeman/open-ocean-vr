﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MakeFishData {
	[MenuItem("Assets/Create/FishData")]
	public static void createMyAsset() {
		FishData fish = ScriptableObject.CreateInstance<FishData>();

		AssetDatabase.CreateAsset(fish, "Assets/Resources/FishData/newFishData.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();
		Selection.activeObject = fish;
	}
}
