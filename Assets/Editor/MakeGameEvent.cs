﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MakeGameEvent {
    [MenuItem("Assets/Create/GameEvent")]
    public static void CreateMyAsset() {
        GameEvent asset = ScriptableObject.CreateInstance<GameEvent>();

        AssetDatabase.CreateAsset(asset, "Assets/Resources/GameEvents/NewGameEvent.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }
}
