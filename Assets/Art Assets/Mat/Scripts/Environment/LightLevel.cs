﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightLevel : MonoBehaviour {
    GameObject player;
    Light sunlight;
    float maxDepth = 10;
    public float defaultLightLevel = 0.1f;

	// Use this for initialization
	void Start () {
        sunlight = GameObject.Find("Sunlight").GetComponent<Light>();
        player = transform.parent.gameObject;
        maxDepth = player.GetComponent<PlayerMovement>().maxDepth;
	}
	
	// Update is called once per frame
	void Update () {
        sunlight.intensity = Mathf.Clamp(((player.transform.position.y / maxDepth)+defaultLightLevel)*2.0f,0,2f);
    }
}
