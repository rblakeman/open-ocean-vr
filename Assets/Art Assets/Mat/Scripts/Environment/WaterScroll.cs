﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterScroll : MonoBehaviour {

    public float scrollSpeed = .25f;
    float offset = 0;
    float rotate = 0;
 
    void Update() {
        offset += (Time.deltaTime * scrollSpeed) / 10.0f;
        GetComponent<MeshRenderer>().material.SetTextureOffset("_MainTex", new Vector2(offset, 0));

    }
}
