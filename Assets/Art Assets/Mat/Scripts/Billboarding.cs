﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboarding : MonoBehaviour {
        GameObject player;
        public Vector3 rotationVector;
        // Use this for initialization
        void Start () {
                player = GameObject.FindGameObjectWithTag("Player");
        }

        // Update is called once per frame
        void Update () {
                Vector3 v = player.transform.position - transform.position;
                v.x = v.z = 0.0f;
                transform.LookAt(player.transform.position - v);
                transform.Rotate(rotationVector);
        }
}
