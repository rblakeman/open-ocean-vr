﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMovement : MonoBehaviour {
    public Transform center;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Vector3.Distance(center.position, transform.position) < 4.0f) {
            transform.Translate(transform.forward*Time.deltaTime, Space.World);
        } else {
            transform.Translate((center.position-transform.position) * Time.deltaTime, Space.World);
        }
	}
}
