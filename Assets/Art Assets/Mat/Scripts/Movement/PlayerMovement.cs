﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    public float speed = 4.0f;
    public float sprintingSpeed = 24.0f;
    public float rotationSpeed = 4.0f;
    public float maxDepth = 10;
    public float minDepth = 1;
    public bool VR = false;
    public bool sprinting = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("Sprint")) {
            sprinting = true;
        } else {
            sprinting = false;
        }
        float inputY = (Input.GetAxisRaw("Vertical"))*((sprinting)?sprintingSpeed:1);
        float newY = Mathf.Clamp(transform.position.y + inputY * Time.deltaTime * speed, minDepth, maxDepth);
        transform.position = new Vector3(transform.position.x, newY, transform.position.z);


        if (!VR) {
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = 5.0f;
            Vector3 pos = Camera.main.ScreenToWorldPoint(mousePos);
            Quaternion rot = Quaternion.LookRotation(pos - Camera.main.transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * rotationSpeed);

        }
	}
}
