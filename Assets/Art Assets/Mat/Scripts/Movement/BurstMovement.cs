﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurstMovement : BasicMovement {
    [Header("Burst Movement Properties")]
    [Tooltip("The duration of movement during a burst")]
    public float burstDuration = 10;
    [Tooltip("The duration of rest between bursts")]
    public float burstCooldown = 3;

    //Burst Movement
    private float lastBurstTime = 0.0f;
    private bool burstMovementCharging = false; //Whether the burst movement is charging (about to begin)
    private float currentBurstDuration = 0.0f;
    private float currentBurstCooldown = 0.0f;

    //Randomize burst cooldown and duration
    new void Initialize (Vector3 origin, ProceduralPathType type, float height, float radius, float minpathradius, float spawnAngleLimit, bool looppath, List<Vector3> p) {
        currentBurstDuration = burstDuration+ Random.Range(0, 0.3f);
        currentBurstCooldown = burstCooldown+ Random.Range(1, 3);

        base.Initialize(origin, type, height, radius, minpathradius, spawnAngleLimit, looppath, p);
    }

    protected override void MoveToTarget () {
        if (!movementAllowed) return;
        //If we can move and burstCooldown time has gone by and we're not already playing the move animation
        if (Time.time - lastBurstTime > currentBurstCooldown && !burstMovementCharging) {
            GetComponent<Animator>().SetTrigger("Move");

            //Prevent us from playing animation again before jet movement
            burstMovementCharging = true;
        }
	}

    //Gets called as an animation event by the movement animation at some point during the animation
    public void Jet() {
        StopAllCoroutines();
        if (!movementAllowed) return;
        //Set chargingJetMovement false so that we may begin charging again after burstCooldown
        burstMovementCharging = false;

        //Reset burst properties
        lastBurstTime = Time.time;
        currentBurstCooldown = burstCooldown + Random.Range(1, 3);
        currentBurstDuration = burstDuration + Random.Range(0, 0.3f);

        //Stop MoveForward if it's already playing (resets burstRemaining) and then start MoveForward
        Vector3 moveDir = moveTarget - transform.position;
        
        StartCoroutine(MoveForward(moveDir));
    }

    //Move forward and rotate toward the lookTarget until burstRemaining is 0
    IEnumerator MoveForward(Vector3 lookTarget) {
        float burstRemaining = currentBurstDuration;

        while (burstRemaining > 0 ) {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(lookTarget, Vector3.up), ((burstDuration-burstRemaining)/burstDuration)*rotationSpeed);

            burstRemaining = Mathf.Max(0, burstRemaining - (Time.deltaTime));
            rb.velocity = transform.forward * (speed * (burstRemaining / burstDuration));
            yield return new WaitForEndOfFrame();
        }
        SlowToStop();
    }

    protected override void SlowToStop() {
        GetComponent<Animator>().SetFloat("Speed", 0);
    }
}
