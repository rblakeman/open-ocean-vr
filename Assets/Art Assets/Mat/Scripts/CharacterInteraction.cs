﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum EventState { Introduction, Active, Lost, Won, Return };
public enum GameEventType { OpenHUD, ActionButton, SelectTarget, Touch, Companion }

public class CharacterInteraction : MonoBehaviour {
    private GameObject eventTarget;

    [HideInInspector]
    public Transform exitPoint;
    [HideInInspector]
    public bool waiting = false;
    [HideInInspector]
    public bool speaking = false;

    private Text speechText;
    private GameObject continueNotice;
    private bool touching = false;
    private bool collecting = false;
    private bool exiting = false;

    //Register ourselves as the character in the GameEventManager
    public void Start() {
        GameEventManager.character = gameObject;

        //Get the world-space text objects
        Transform canvas = transform.Find("Canvas");
        continueNotice = canvas.Find("ContinueNotice").gameObject;
        SetContinueNotice(false);
        speechText = canvas.Find("SpeechText").GetComponent<Text>();
    }

    public void Initialize(Transform exitPoint) {
        transform.tag = "Character";
        this.exitPoint = exitPoint;
    }

    public void SetContinueNotice(bool active) {
        continueNotice.SetActive(active);
    }

    public GameObject GetEventTarget() {
        return eventTarget;
    }

    public bool IsTouching() {
        return touching;
    }

    public bool IsCollecting() {
        return collecting;
    }

    public void TouchTarget(GameObject target) {
        //Set that target as our eventTarget, minimize our approachDistance and then get a path to the target position
        eventTarget = target;
        touching = true;
        GetComponent<BasicMovement>().approachDistance = 2;
        GetComponent<BasicMovement>().GetPathToTarget(transform.position, eventTarget, 2);
    }

    public void CollectTarget(GameObject target) {
        //Set that target as our eventTarget, minimize our approachDistance and then get a path to the target position
        eventTarget = target;
        collecting = true;
        GetComponent<BasicMovement>().approachDistance = 10;
        GetComponent<BasicMovement>().GetPathToTarget(transform.position, eventTarget, 10);
    }

    //Reduce the target's scale over time
    public void OnTouchingTarget(GameObject target) {
        StartCoroutine(MinimizeTarget(target));
    }

    //Set the companion creatures' follow target to this gameObject
    public void OnCollectingCompanion(GameObject target) {
        Debug.Log("OnCollecting");
        //Stop the target
        target.GetComponent<Rigidbody>().isKinematic = false;
        if (target.GetComponent<BurstMovement>())
            target.GetComponent<BurstMovement>().enabled = true;
        if (target.GetComponent<BasicMovement>())
            target.GetComponent<BasicMovement>().enabled = true;

        target.GetComponent<BasicMovement>().ResumeMovement();
        target.GetComponent<BasicMovement>().SetFollowTarget(gameObject);
        collecting = false;
        touching = false;
        ReturnToPlayer();
    }

    //Reduce the target's size until it's nearly, then destroy the target and return to the player
    private IEnumerator MinimizeTarget(GameObject target) {
        float scale = target.transform.localScale.x;
        while (target.transform.localScale.x > 0.1f) {
            yield return null;
            target.transform.localScale = new Vector3(target.transform.localScale.x - Time.deltaTime, target.transform.localScale.y - Time.deltaTime, target.transform.localScale.z - Time.deltaTime);
        }
        Destroy(target.gameObject);
        touching = false;
        ReturnToPlayer();
    }

    //Get a path to the player and move along it
    private void ReturnToPlayer() {
        GetComponent<BasicMovement>().ResumeMovement();
        GameEventManager.CompleteEvent();
        GetComponent<BasicMovement>().approachDistance = 3;
        GetComponent<BasicMovement>().GetPathToPlayer(transform.position);
        Debug.Log("Returning to player?");
    }

    //Get a path to the player and move along it
    public void MoveToPlayer() {
        GetComponent<BasicMovement>().ResumeMovement();
        GetComponent<BasicMovement>().approachDistance = 3;
        GetComponent<BasicMovement>().GetPathToPlayer(transform.position);
        Debug.Log("Returning to player?");
    }

    public void Speak(string speech, float duration, bool activateOnFinish, bool completeOnFinish = false) {
        StartCoroutine(Talk(speech, duration, activateOnFinish, completeOnFinish));
    }

    protected IEnumerator Talk(string speech, float duration, bool activateOnFinish, bool completeOnFinish = false) {
        speechText.text = speech;
        speaking = true;

        if (duration <= -2) {
            waiting = false;
            yield return null;
            speechText.text = speech;
            speaking = false;
        } else if (duration <= 0 && duration > -2) {
            waiting = true;
            while (waiting)
                yield return new WaitForEndOfFrame();
            speechText.text = "";
            speaking = false;
        } else {
            yield return new WaitForSeconds(duration);
            speechText.text = "";
            speaking = false;
        }

        if (activateOnFinish)
            GameEventManager.ActivateEvent();

        if (completeOnFinish)
            ReturnToPlayer();
    }

    public void ReachedDestination() {
        if (exiting) {
            Destroy(gameObject);
        }
        GameEventManager.OnReachedDestination(this);
    }

    public void Leave() {
        GetComponent<BasicMovement>().approachDistance = 5;
        GetComponent<BasicMovement>().GetPathToTarget(transform.position, exitPoint.gameObject, 1);
    }
}
