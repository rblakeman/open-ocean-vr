﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
    [Header("Spawn Details")]
    public GameObject creaturePrefab;
    [Tooltip("Number of creatures to spawn.")]
    public int spawnCount;
    [Tooltip("The maximum spawn radius.")]
    public float radius = 20.0f;
    [Tooltip("If this is a Random path type, what is the minimum spawn radius?")]
    public float minRadius = 1.0f;
    [Tooltip("Height of the spawn area.")]
    public float height = 5.0f;

    [Header("Pathfinding Options")]
    [Tooltip("Should the AI loop around this path indefinitely?")]
    public bool looping = true;
    [Tooltip("Should the AI spawn immediately?")]
    public bool spawnOnStart = true;
    [Tooltip("Is this a single character spawner?")]
    public bool characterSpawner = false;
    [Tooltip("The amount of time to delay spawning this creature.")]
    public float spawnDelay = 0.0f;
    [Tooltip("How far around the spawner the AI will randomly spawn. Set to a lower number to create schools of fish.")]
    [Range(0,360.0f)]
    public float spawnAngleLimit = 360.0f; //Make this smaller to cause the creatures to spawn in a swarm!
    [Range(0, 99999.9f)]
    [Tooltip("How close should this character move to the player?")]
    public float approachDistance = 5.0f;
    [Tooltip("Color of this spawner's maximum radius gizmo.")]
    public Color radiusGizmoColor = Color.white;
    [Tooltip("Color of this spawner's minimum radius gizmo (if applicable).")]
    public Color minRadiusGizmoColor = Color.grey;

    [Header("Path Properties")]
    [Tooltip("Circle path types are perfectly round loops, Random path types are loops with random distances from the center and Approach will cause an object to immediately approach a target.")]
    public ProceduralPathType pathType;
    [Tooltip("Place navigation node objects here to assign a pre-determined, customized path.")]
    public List<GameObject> pathObjects;
    [Tooltip("For a character spawner, this is the point the character will exit to when the player finishes the events.")]
    public GameObject exitPoint;
    
    protected List<GameObject> spawnedCreatures;
    // Use this for initialization
    protected void Start () {
        if (spawnOnStart) {
            Spawn();
        } else {
            StartCoroutine(YieldSpawn());
        }
	}

    protected IEnumerator YieldSpawn() {
        yield return new WaitForSeconds(spawnDelay);
        Spawn();
    }

    List<Vector3> ConvertPath() {
        //Convert path gameObjects to Vector3 positions (gameObjects are easier to assign in the editor)
        List<Vector3> path = new List<Vector3>();
        foreach (GameObject obj in pathObjects)
        {
            path.Add(obj.transform.position);
        }
        return path;
    }

    void Spawn() {
        List<Vector3> path = ConvertPath();

        if (characterSpawner)
            spawnCount = 1;

        for(int i = 0; i < spawnCount; i++) {
            //Spawn the creature, initialize it's path generating function with this spawner's values and then add the spawned creature to the list of spawned creatures
            spawnedCreatures = new List<GameObject>();
            GameObject creature = Instantiate(creaturePrefab, transform.position, Quaternion.identity);
            creature.name = creaturePrefab.name;

            if (!characterSpawner) {
                float randHeight = Random.Range(transform.position.y, transform.position.y + height);
                creature.GetComponent<BasicMovement>().Initialize(transform.position, pathType, randHeight, radius, Mathf.Clamp(minRadius, 0, radius - 0.1f), spawnAngleLimit, looping, path);
                creature.GetComponent<Animator>().speed = Random.Range(0.25f, 1.5f);
                spawnedCreatures.Add(creature);
            } else {
                creature.GetComponent<BasicMovement>().Initialize(transform.position, pathType, transform.position.y, 0, 0, 0, looping, path);
                creature.GetComponent<Animator>().speed = 1.0f;
                creature.GetComponent<CharacterInteraction>().Initialize(exitPoint.transform);
                spawnedCreatures.Add(creature);
            }
        }
    }

    void OnDrawGizmosSelected() {
        if (!characterSpawner) {
            Gizmos.color = radiusGizmoColor;
            DrawCircle(radius, 0, true);
            DrawCircle(radius, height, false);
            if (pathType == ProceduralPathType.Random) {
                Gizmos.color = minRadiusGizmoColor;
                DrawCircle(minRadius, 0, true);
                DrawCircle(minRadius, height, false);
            }
        }
    }

    void OnDrawGizmos() {
        if (characterSpawner) {
            Gizmos.color = radiusGizmoColor;
            DrawCircle(0.25f, 0, false, true);
            DrawCircle(0.25f, 0, false, false);
            DrawCircle(0.1f, 0, false, true);
            DrawCircle(0.1f, 0, false, false);
        }
    }

    void DrawCircle(float r, float h, bool drawSides, bool rotate = false) {
        float theta = 0;
        float x = r * Mathf.Cos(theta);
        float y = r * Mathf.Sin(theta);
        Vector3 pos = transform.position + new Vector3(x, h, y);
        if (rotate) {
            pos = transform.position + new Vector3(x, y, h);
        }
        Vector3 newPos = pos;
        Vector3 lastPos = pos;
        for (theta = 0.1f; theta < Mathf.PI * 2; theta += 0.1f) {
            x = r * Mathf.Cos(theta);
            y = r * Mathf.Sin(theta);
            
            newPos = transform.position + new Vector3(x, h, y);
            if (rotate) {
                newPos = transform.position + new Vector3(x, y, h);
            }
            Gizmos.DrawLine(pos, newPos);
            if (drawSides) {
                Gizmos.DrawLine(pos, pos+new Vector3(0,height,0));
            }
            pos = newPos;
        }
        Gizmos.DrawLine(pos, lastPos);
    }
}
