﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/Unlit/SwimmingVertexColor"
{
	Properties
	{
		_MainColor ("Color", Color) = (0,0,0,0)
		_Amplitude("_Amplitude", Float) = 1
		_Period("_Period", Float) = 1
		_PhaseShift("_PhaseShift", Float) = 0
		_VerticalShift("_VerticalShift", Float) = 0

	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			fixed4 _MainColor;
			float _Amplitude;
			float _Period;
			float _PhaseShift;
			float _VerticalShift;


			float worldRandom(float3 worldPos){
				float rand = 0;
				rand += frac(worldPos.x);
				rand += frac(worldPos.y);
				rand += frac(worldPos.z);

				rand = clamp(rand, -1.0, 1.0);

				return rand;
			}

			v2f vert (appdata v)
			{
				v2f o;
				//o.vertex = UnityObjectToClipPos(v.vertex);
				//o.uv = TRANSFORM_TEX(v.uv, _MainColor);

				//o.vertex.x += _Amplitude * sin(_Period * (v.vertex.x+_Time) + _PhaseShift) + _VerticalShift;

				float4 worldPos = mul(unity_ObjectToWorld, v.vertex);

				float yaw = sin(_Time.x) * (sin(v.vertex.z/v.vertex.w) * cos(3.14 * v.vertex.z/v.vertex.w));

				worldPos.x += yaw * _Amplitude * (sin(_Period * _Time.x) + (worldRandom(worldPos)+_PhaseShift)) + _VerticalShift; //side to side
				//worldPos.z += yaw * _Amplitude * (sin(_Period * _Time.x) + _PhaseShift) + _VerticalShift;
				o.vertex = mul(UNITY_MATRIX_VP, worldPos);

				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = _MainColor;
				return col;
			}
			ENDCG
		}
	}
}
