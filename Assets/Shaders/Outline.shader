﻿Shader "Custom/Unlit/Outline"
{
	Properties
	{
		_Color("Main Color", Color) = (.5,.5,.5,1)
		_Texture ("Texture", 2D) = "white" {}
		_Normal ("Bumpmap", 2D) = "bump" {}
		_OutlineColor("Outline Color", Color) = (0,0,0,1)
		_OutlineWidth("Outline Width", Range(1.0,5.0)) = 1.01
	}

	CGINCLUDE
	#include "UnityCG.cginc"

	struct appdata
	{
		float4 vertex : POSITION;
		float3 normal : NORMAL;
	};

	struct v2f
	{
		float4 pos : POSITION;
		float3 normal : NORMAL;
	};

	sampler2D _Texture;
	sampler2D _Normal;
	float _OutlineWidth;
	float4 _OutlineColor;

	v2f vert(appdata v)
	{
		v.vertex.xyz *= _OutlineWidth;
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		return o;
	}

	ENDCG

	SubShader
	{
		Tags { "Queue" = "Transparent" }

		Pass //Outline
		{
			ZWrite Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			half4 frag(v2f i) : COLOR
			{
				return _OutlineColor;
			}
			ENDCG
		}

		Pass //Texture
		{
			ZWrite On

			Material
			{
				Diffuse[_Color]
				Ambient[_Color]
			}

			Lighting On

			SetTexture[_Texture]
			{
				ConstantColor[_Color]
			}

			SetTexture[_Texture]
			{
				Combine previous * primary DOUBLE
			}
		}
	}
}
