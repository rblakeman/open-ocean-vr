﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class FishData : ScriptableObject {
	[SerializeField]
	public string fishname;
	[SerializeField]
	public string scientificname;
	[SerializeField]
	public string size;
	[SerializeField]
	public string habitat;
	[SerializeField]
	public string animaltype;
	[SerializeField]
	public string diet;
	[SerializeField]
	public string relative;
	[SerializeField]
	public string range;
	[SerializeField]
	public Sprite icon;
	[SerializeField]
	public bool discovered;
	[SerializeField]
	public bool isUsed;
}
