Bluestreak Cleaner Wrasse
Labroides dimidatus
Up to 8" long
Coral reefs, seafloor, kelp beds
Fishes
Feeds primarily on parasites found on larger fish, cleaning them in the process
N/A
Throughout the Indo-Pacific, found in shallower waters