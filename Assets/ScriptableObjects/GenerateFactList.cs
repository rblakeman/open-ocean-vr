﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateFactList : MonoBehaviour {

	public static Dictionary<string, FishData> fishfacts = new Dictionary<string, FishData>();

	// Use this for initialization
	void Awake () {
		Object[] o = Resources.LoadAll("FishData", typeof(FishData));
		foreach (Object obj in o)
		{
			FishData f = (FishData)obj;
			f.discovered = false;
			if (f.isUsed)
				fishfacts.Add(f.name, f);
		}
        //Debug.Log("first");
	}
	
	// Update is called once per frame
	void Update () {
		/*FishData fish;
		fishfacts.TryGetValue("GreenSeaTurtle", out fish);
		Debug.Log(fish.fishname);*/
	}
}
