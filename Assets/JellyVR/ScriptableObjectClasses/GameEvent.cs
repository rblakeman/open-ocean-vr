﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameEvent : ScriptableObject {
    [SerializeField]
    public string eventName;
    [SerializeField]
    public GameEventType eventType;
    [SerializeField]
    public List<string> acceptableInput;
    [SerializeField]
    public string introduction;
    [SerializeField]
    public string wrong;
    [SerializeField]
    public string right;
    //[SerializeField]
    //public string successConclusion;
    //[SerializeField]
    //public string failureConclusion;
    [SerializeField]
    public string instructions;
    [SerializeField]
    public string description;
    [SerializeField]
    public string hint;
    [SerializeField]
    public Sprite eventIcon;
}