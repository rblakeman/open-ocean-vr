﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameEventManager : MonoBehaviour {
    //All of the GameEvents in the Resources/GameEvents folder
    private static Dictionary<string, GameEvent> gameEvents = new Dictionary<string, GameEvent>();

    public static string currentEventName;
    public static string selectedObjectName = "Jelly";
    public static GameObject selectedGameObject;

    public static EventState eventState;
    public static GameEvent currentEvent;
    public static GameObject character;
    private static bool finalEvent = false;
    public static GameEventManager singleton;
    
    private bool tutorialMode = true;

    public GameEvent startingGameEvent;
    public GameEvent finalGameEvent;

    [Tooltip("This is an ordered (0 being the first) list of tutorial game events that will play before any main events")]
    public List<string> tutorialEvents = new List<string>();

    //Populated via non-tutorial GameEvent ScriptableObjects in Initialize()
    [Tooltip("This is an ordered (0 being the first) list of game events that will play after all of the tutorial game events. If this array is left empty, it will be filled with all of the game events in the Resources/GameEvents folder, otherwise only the game events in the list will be played.")]
    public List<string> mainEvents = new List<string>();

    static private int mainEventCount = 0;
    static private int completedEventCount = 0;

    //Character transitions to Introduction state when it reaches player, then transitions to Active event when player presses action button
    //then transitions to correct choice or wrong choice by target selection + action button, then transitions to returning to player when it reaches the target selection
    #region PlayerIntroduction
    public static void StartEvent() {
        CharacterInteraction chrctr = GameEventManager.character.GetComponent<CharacterInteraction>();
        chrctr.SetContinueNotice(true);
        chrctr.Speak(currentEvent.introduction, -1f, true);
    }
    #endregion

    #region ActiveEventChoices
    public static void CorrectChoice() {
        switch (currentEvent.eventType) {
            case GameEventType.Touch:
                //Go touch the target
                GameEventManager.character.GetComponent<CharacterInteraction>().Speak(currentEvent.right, 5.0f, false);
                GameEventManager.character.GetComponent<CharacterInteraction>().TouchTarget(selectedGameObject);

                //Stop the target
                selectedGameObject.GetComponent<Rigidbody>().isKinematic = true;
                selectedGameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
                if (selectedGameObject.GetComponent<BurstMovement>())
                    selectedGameObject.GetComponent<BurstMovement>().enabled = false;
                if (selectedGameObject.GetComponent<BasicMovement>())
                    selectedGameObject.GetComponent<BasicMovement>().enabled = false;
                break;
            case GameEventType.Companion:
                //Go touch the target
                GameEventManager.character.GetComponent<CharacterInteraction>().Speak(currentEvent.right, 5.0f, false);
                GameEventManager.character.GetComponent<CharacterInteraction>().CollectTarget(selectedGameObject);

                //Stop the target
                selectedGameObject.GetComponent<Rigidbody>().isKinematic = true;
                selectedGameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
                if (selectedGameObject.GetComponent<BurstMovement>())
                    selectedGameObject.GetComponent<BurstMovement>().enabled = false;
                if (selectedGameObject.GetComponent<BasicMovement>())
                    selectedGameObject.GetComponent<BasicMovement>().enabled = false;
                break;
            default:
                GameEventManager.character.GetComponent<CharacterInteraction>().Speak(currentEvent.right, 5.0f, false, true);
                break;
        }
    }

    public static void WrongChoice() {
        switch (currentEvent.eventType) {
            case GameEventType.Touch:
                GameEventManager.character.GetComponent<CharacterInteraction>().Speak(currentEvent.wrong, 5.0f, false);
                break;
            case GameEventType.Companion:
                GameEventManager.character.GetComponent<CharacterInteraction>().Speak(currentEvent.wrong, 5.0f, false);
                break;
            case GameEventType.OpenHUD:
                GameEventManager.character.GetComponent<CharacterInteraction>().Speak(currentEvent.wrong, 5.0f, false);
                break;
            case GameEventType.SelectTarget:
                GameEventManager.character.GetComponent<CharacterInteraction>().Speak(currentEvent.wrong, 5.0f, false);
                break;
            case GameEventType.ActionButton:
                GameEventManager.character.GetComponent<CharacterInteraction>().Speak(currentEvent.wrong, 5.0f, false);
                break;
            default:
                GameEventManager.character.GetComponent<CharacterInteraction>().Speak(currentEvent.wrong, 5.0f, false);
                break;
        }
    }
    #endregion

    #region ReturningToPlayer
    //When the character has completed the event and is returning to the player
    public static void NextEvent(bool won) {
        singleton.SpeakConclusion(won);
    }

    private void SpeakConclusion(bool won) {
        //EndEvent(won);
        ContinueToNext();
    }

    /*private void EndEvent(bool won, float duration = -1f) {
        if (won)
            GameEventManager.character.GetComponent<CharacterInteraction>().Speak(currentEvent.successConclusion, duration, true);
        else
            GameEventManager.character.GetComponent<CharacterInteraction>().Speak(currentEvent.failureConclusion, duration, true);
    }*/

    private void ContinueToNext() {
        string evtName = null;
        //If we're in tutorial mode, go to the next tutorial event and remove it
        //from the tutorial list until there are no more tutorials.
        //Then go the a random event
        selectedGameObject = null; //prevents events from skipping when that fish had been last scanned
        selectedObjectName = null; //^
        if (tutorialMode) {
            if (tutorialEvents.Count > 0) {
                evtName = tutorialEvents[0];
            } else {
                tutorialMode = false;
            }
        }
        if (finalEvent) {
            character.GetComponent<CharacterInteraction>().Leave();
            return;
        }

        currentEventName = GetEvent(evtName);
        currentEvent = GetCurrentEvent();
        InitializeEvent();
        StartEvent();
        if(tutorialEvents.Count > 0) {
            tutorialEvents.RemoveAt(0);
        }
    }

    #endregion

    #region ReachedDestinationStateTransitions
    public static void OnReachedDestination(CharacterInteraction charInteraction) {
        switch (GameEventManager.eventState) {
            case EventState.Introduction:
                GameEventManager.StartEvent();
                break;
            case EventState.Active:
                GameObject evtTarget = charInteraction.GetEventTarget();
                if (evtTarget && charInteraction.IsTouching()) {
                    charInteraction.OnTouchingTarget(evtTarget);
                } else if(evtTarget && charInteraction.IsCollecting()) {
                    Debug.Log("Collecting");
                    charInteraction.OnCollectingCompanion(evtTarget);
                }
                break;
            case EventState.Return:
                GameEventManager.NextEvent(true);
                break;
        }
    }
    #endregion

    #region InputHandling
    //Input handling
    void Update() {
        //MLT: This should be called by RYAN's script when the selection trigger is pressed. 
        //IMPLEMENTATION: Pass a Transform in to TargetSelection to override mouse click targetting

        //Should be replaced with VR arm raycasting, just call SetCurrentSelectedObject on a raycast hit
        /*if (Input.GetButtonUp("Fire1")) {
            TargetSelection();
        }*

        //MLT: This should be called by JASON's script when the HUD menu button is pressed
        //IMPLEMENTATION: Pass a bool in to OpenMenuHUD to distinguish between HUD opening and closing
        /*if (Input.GetButtonUp("Menu")) {
            OpenMenuHUD();
        }*/

        //MLT: This MIGHT be called by one of the other two scripts.
        //Set the input name to the Action button
        /*if (Input.GetButtonUp("Fire2")) {
            ActionButtonPressed();
        }*/
    }

    public static void TargetSelection(Transform hitObj) { //(Transform hitObj=null) {
        CharacterInteraction chrctr = GameEventManager.character.GetComponent<CharacterInteraction>();
        /*if (hitObj == null) {
            Vector3 mouse = Input.mousePosition;

            Vector3 shootPoint = Camera.main.ScreenToWorldPoint(mouse);
            Ray ray = Camera.main.ScreenPointToRay(mouse);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1 << 8)) {
                SetCurrentSelectedObject(hit.transform.name, hit.transform.gameObject);
            }
        } else {*/
            SetCurrentSelectedObject(hitObj.name, hitObj.gameObject);
            Debug.Log("hitObj name: " + hitObj.name + " hitObj.gameobject: " + hitObj.gameObject);
        //}

        switch (eventState) {
            case EventState.Introduction:
                break;
            //If the player presses the selection trigger during an event & it's a tutorial event for selecting fish, it's the correct choice
            case EventState.Active:
                switch (currentEvent.eventType) {
                    case GameEventType.Touch:
                        break;
                    case GameEventType.Companion:
                        break;
                    case GameEventType.OpenHUD:
                        break;
                    case GameEventType.SelectTarget:
                        /*if (!chrctr.speaking) {
                            CorrectChoice();
                        }*/
                        break;
                    default:
                        break;
                }
                break;
        }
    }
    
    //Called when the player selects a new target
    public static void SetCurrentSelectedObject(string objectName, GameObject gObj) {
        selectedObjectName = objectName;
        selectedGameObject = gObj;
    }

    public static void OpenMenuHUD(bool open=true) {
        CharacterInteraction chrctr = GameEventManager.character.GetComponent<CharacterInteraction>();
        if (open) {
            GameEventType type = currentEvent.eventType;
            
            switch(type){
                case GameEventType.OpenHUD:
                switch (eventState) {
                    //If the player presses the menu button during an event & it's a tutorial event for opening the menu, it's the correct choice
                    case EventState.Active:
                        if (!chrctr.speaking) {
                            CorrectChoice();
                        }
                        break;
                }
                break;
            }
        }
    }

    public static void ActionButtonPressed() {
        CharacterInteraction chrctr = GameEventManager.character.GetComponent<CharacterInteraction>();
        switch (eventState) {
            case EventState.Introduction:
                if (chrctr.waiting)
                    chrctr.waiting = false;

                chrctr.SetContinueNotice(false);
                chrctr.Speak(currentEvent.instructions, -2f, false);
                break;
            case EventState.Active:
                switch (currentEvent.eventType) {
                    case GameEventType.Touch:
                        if (!chrctr.waiting && !chrctr.IsTouching()) {
                            //If we have a selection
                            if (selectedObjectName != "None" && selectedObjectName != "") {
                                //If it's acceptable
                                string printout = "HUNT acceptable: ";
                                foreach(string i in currentEvent.acceptableInput)
                                {
                                    printout += i;
                                }
                                Debug.Log(printout + " - selected: " + selectedObjectName);
                                if (currentEvent.acceptableInput.Contains(selectedObjectName)) {
                                    Debug.Log("HUNT correctChoice()");
                                    CorrectChoice();
                                    //If it's NOT acceptable
                                } else {
                                    Debug.Log("HUNT wrongChoice()");
                                    WrongChoice();
                                }
                            }
                        } else {
                            chrctr.waiting = false;
                        }
                        break;
                    case GameEventType.Companion:
                        if (!chrctr.waiting && !chrctr.IsCollecting()) {
                            //If we have a selection
                            if (selectedObjectName != "None" && selectedObjectName != "") {
                                //If it's acceptable\
                                string printout = "COMPANION acceptable: ";
                                foreach(string i in currentEvent.acceptableInput)
                                {
                                    printout += i;
                                }
                                Debug.Log(printout + " - selected: " + selectedObjectName);
                                if (currentEvent.acceptableInput.Contains(selectedObjectName)) {
                                    Debug.Log("COMPANION correctChoice()");
                                    CorrectChoice();
                                    //If it's NOT acceptable
                                } else {
                                    Debug.Log("COMPANION wrongChoice()");
                                    WrongChoice();
                                }
                            }
                        } else {
                            chrctr.waiting = false;
                        }
                        break;
                    case GameEventType.SelectTarget:
                        if (!chrctr.waiting) {
                            //If we have a selection
                            if (selectedObjectName != "None" && selectedObjectName != "") {
                                //If it's acceptable\
                                string printout = "SELECTTARGET acceptable: ";
                                foreach(string i in currentEvent.acceptableInput)
                                {
                                    printout += i;
                                }
                                Debug.Log(printout + " - selected: " + selectedObjectName);
                                if (currentEvent.acceptableInput.Contains(selectedObjectName)) {
                                    Debug.Log("SELECTTARGET correctChoice()");
                                    CorrectChoice();
                                    //If it's NOT acceptable
                                } else {
                                    Debug.Log("SELECTTARGET wrongChoice()");
                                    WrongChoice();
                                }
                            }
                        } else {
                            chrctr.waiting = false;
                        }
                        break;
                    case GameEventType.ActionButton:
                        if (!chrctr.speaking) {
                            CorrectChoice();
                        }
                        break;
                }
                break;
        }
    }

    #endregion

    #region CurrentEventInitialization
    //Initialize the current event to the Start event
    void Start() {
        singleton = this;
        currentEventName = startingGameEvent.name;
        Initialize();
        currentEvent = GetCurrentEvent();
        InitializeEvent();
    }

    public void Initialize() {
        //Get all the GameEvent ScriptableObjects from the Resources/GameEvents folder
        Object[] o = Resources.LoadAll("GameEvents", typeof(GameEvent));
        //If the designer has assigned mainEvents, don't randomize them after the tutorial events
        bool randomizeMainEvents = mainEvents.Count <= 0;
        foreach (Object obj in o) {
            //Add all the GameEvent ScriptableObjects to the dictionary
            GameEvent gameEvent = (GameEvent)obj;
            gameEvents.Add(gameEvent.name, gameEvent);

            //For each GameEvent, if it's not the Start event && we're set to randomize mainEvents
            if (gameEvent != startingGameEvent && gameEvent!=finalGameEvent && randomizeMainEvents) {
                //And if it's not a tutorial event, add it to the mainEvents
                if (!tutorialEvents.Contains(gameEvent.name)) {
                    mainEvents.Add(gameEvent.name);
                    mainEventCount++;
                }
            }
        }
    }
    #endregion

    #region CurrentEventState
    //Get the current GameEvent object from the dictionary
    public static GameEvent GetCurrentEvent() {
        GameEvent gameEvent;
        gameEvents.TryGetValue(currentEventName, out gameEvent);

        if (currentEventName == singleton.finalGameEvent.name)
            return singleton.finalGameEvent;

        return gameEvent;
    }

    //Return the specified event name or a random event
    private static string GetEvent(string eventName = null) {
        if (eventName == null) {
            string[] keys = singleton.mainEvents.ToArray();

            if (keys.Length > 0 && completedEventCount<mainEventCount) {
                eventName = keys[Random.Range(0, keys.Length)];
                singleton.mainEvents.Remove(eventName);
            } else {
                finalEvent = true;
                eventName = singleton.finalGameEvent.name;
            }
        }

        return eventName;
    }

    protected static void SetState(EventState state) {
        eventState = state;
    }

    //Set current event to Introduction state
    public static void InitializeEvent() {
        SetState(EventState.Introduction);
    }

    //Set the current event to Active state
    public static void ActivateEvent() {
        SetState(EventState.Active);
    }

    //Set the current event to the Return state (and remove it from potential game events)
    public static void CompleteEvent() {
        if(singleton.mainEvents.Contains(currentEventName))
            completedEventCount++;
        SetState(EventState.Return);
    }

    #endregion
}
