# Open Ocean VR

##### Final project from Spring 2018 CST-499 Capstone.

> Unity Version 2017.3.0f3

## Credits

Designer: [Emily Turnage](https://www.linkedin.com/in/emturnage/)

Developers: [Ryan Blakeman](https://ryanblakeman.com/), [Mathew Tomberlin](https://www.linkedin.com/in/mathew-tomberlin-4b570aa7/), [Jason Ferrer](https://www.linkedin.com/in/jason-ferrer-579811137/)

